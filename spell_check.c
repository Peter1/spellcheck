#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "dictionary.h"

#define MAX_CMD_LEN 128

// A helper function to spell check a specific file
// 'file_name': Name of the file to spell check
// 'dict': A dictionary containing correct words
int spell_check_file(const char *file_name, const dictionary_t *dict) {
  FILE *file;
  char word[MAX_WORD_LEN + 1];
  char letter;
  int is_word;
  char delim;

  file = fopen(file_name, "r");

  if (file == NULL) {
    return 1;
  }

  int i = 0;
  while (fscanf(file, "%c", &letter) != EOF) {
		// Check for spacing characters
    if (!isspace(letter)) {
      word[i] = letter;
      ++i;
    }

    else {
      word[i] = '\0'; // NULL terminate string
      is_word = dict_find(dict, word);
      delim = letter;

      if (!is_word) {
        printf("%s[X]%c", word, delim);
      } else {
        printf("%s%c", word, delim);
      }
      i = 0;
    }
  }
  printf("\n");
  fclose(file);
  return 0;
}

/*
 * This is in general *very* similar to the list_main file seen in lab
 */
int main(int argc, char **argv) {
  dictionary_t *dict = create_dictionary();

	// Command line args
  if (argc == 2) {
    dictionary_t *new_dict = read_dict_from_text_file(argv[1]);
    if (new_dict == NULL) {
      printf("Failed to read dictionary from text file\n");
      dict_free(dict);
      return 1;
    } else {
      printf("Dictionary successfully read from text file\n");
      dict_free(dict);
      dict = new_dict;
    }
  } else if (argc == 3) {
    dictionary_t *new_dict = read_dict_from_text_file(argv[1]);
    if (new_dict == NULL) {
      printf("Failed to read dictionary from text file\n");
      dict_free(dict);
      return 1;
    } else {
      printf("Dictionary successfully read from text file\n");
      int error = spell_check_file(argv[2], new_dict);
      dict_free(new_dict);
      dict_free(dict);
      if (error) {
        printf("Spell check failed\n");
        return 1;
      }
      return 0;
    }
  } else if (argc > 3) {
    printf("Unsupported args");
  }

  char cmd[MAX_CMD_LEN];

  printf("CSCI 2021 Spell Check System\n");
  printf("Commands:\n");
  printf("  add <word>:              adds a new word to dictionary\n");
  printf("  lookup <word>:           searches for a word\n");
  printf("  print:                   shows all words currently in the "
         "dictionary\n");
  printf("  load <file_name>:        reads in dictionary from a file\n");
  printf("  save <file_name>:        writes dictionary to a file\n");
  printf("  check <file_name>: spell checks the specified file\n");
  printf("  exit:                    exits the program\n");

	// Read commands
  while (1) {
    printf("spell_check> ");

    if (scanf("%s", cmd) == EOF) {
      printf("\n");
      break;
    }

    if (strcmp("exit", cmd) == 0) {
      break;
    }

    else if (strcmp("print", cmd) == 0) {
      dict_print(dict);
    }

    else if (strcmp("add", cmd) == 0) {
      scanf("%s", cmd);
      int error = dict_insert(dict, cmd);
      if (error) {
        printf("Error adding to dict\n");
      }
    }

    else if (strcmp("lookup", cmd) == 0) {
      scanf("%s", cmd);
      int is_word = dict_find(dict, cmd);
      if (!is_word) {
        printf("'%s' not found\n", cmd);
      } else {
        printf("'%s' present in dictionary\n", cmd);
      }
    }

    else if (strcmp("load", cmd) == 0) {
      scanf("%s", cmd);
      dictionary_t *new_dict = read_dict_from_text_file(cmd);
      if (new_dict == NULL) {
        printf("Failed to read dictionary from text file\n");
      } else {
        printf("Dictionary successfully read from text file\n");
        dict_free(dict);
        dict = new_dict;
      }
    }

    else if (strcmp("save", cmd) == 0) {
      scanf("%s", cmd);
      int error = write_dict_to_text_file(dict, cmd);
      if (error) {
        printf("error writing to file");
      } else {
        printf("Dictionary successfully written to text file\n");
      }
    }

    else if (strcmp("check", cmd) == 0) {
      scanf("%s", cmd);

      int error = spell_check_file(cmd, dict);

      if (error) {
        printf("Spell check failed\n");
      }
    }

    else {
      printf("Unknown command %s\n", cmd);
    }
  }

  dict_free(dict);
  return 0;
}
