#include "dictionary.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Resize factor incremented each time hash table is resized. It is then
// multiplied by the initial size of the hash table. This will scale the
// hash table by 1301 each time a resize is needed.
unsigned int resize_factor = 0;

table_t *create_table() {
	// Increment resize each time table is created
  ++resize_factor;
  table_t *table = (table_t *)malloc(sizeof(table_t));

  if (table == NULL) {
    return NULL;
  }

  table->length = INITIAL_HASH_TABLE_SIZE * resize_factor;
  table->array = (list_node_t **)calloc(table->length, sizeof(list_node_t *));

  if (table->array == NULL) {
    free(table);
    return NULL;
  }

  return table;
}

dictionary_t *create_dictionary() {
  dictionary_t *dict = (dictionary_t *)malloc(sizeof(dictionary_t));

  if (dict == NULL) {
    return NULL;
  }

  dict->table = create_table();

  if (dict->table == NULL) {
    dict_free(dict);
    return NULL;
  }

  dict->size = 0;
  return dict;
}

// Uses the djb2 algorithm from http://www.cse.yorku.ca/~oz/hash.html
int hash_code(const char *word) {
  int hash = 5381;
  int c;

  while ((c = *word++)) {
    hash = ((hash << 5) + hash) + c; // hash * 33 + c
  }

  return hash > 0 ? hash : -1 * hash;
}

int dict_insert(dictionary_t *dict, const char *word) {
  int index = hash_code(word) % dict->table->length;

  list_node_t *node = (list_node_t *)malloc(sizeof(list_node_t));
  if (node == NULL) {
    return -1;
  }

  strcpy(node->word, word);
  node->next = NULL;

  if (dict->table->array[index] == NULL) {
    dict->table->array[index] = node;
  } else {
    node->next = dict->table->array[index];
    dict->table->array[index] = node;
  }
  ++dict->size;

  // Calculate the load factor to resize
  float lf = dict->size / dict->table->length;

  if (lf > 0.8) {
    dict->table = resize_table(dict->table);
  }

  return 0;
}

table_t *resize_table(table_t *original) {
  table_t *new_table = create_table();

  if (new_table == NULL) {
    return NULL;
  }

  int index;

  for (unsigned int i = 0; i < original->length; ++i) {
    if (original->array[i] != 0) {

      list_node_t *head = original->array[i];

      while (head != NULL) {
        list_node_t *next = head->next;
        index = hash_code(head->word) % new_table->length;

        if (new_table->array[index] == NULL) {
          new_table->array[index] = head;
          head->next = NULL;
        } else {
          head->next = new_table->array[index];
          new_table->array[index] = head;
        }

        head = next;
      }
    }
  }

  free(original->array);
  free(original);
  return new_table;
}

int dict_find(const dictionary_t *dict, const char *query) {
  int index = hash_code(query) % dict->table->length;
  list_node_t *tmp_node = dict->table->array[index];

  while (tmp_node != NULL) {
    if (strcmp(tmp_node->word, query) == 0) {
      return 1;
    }
    tmp_node = tmp_node->next;
  }

  free(tmp_node);
  return 0;
}

void dict_print(const dictionary_t *dict) {
  for (unsigned int i = 0; i < dict->table->length; ++i) {
    if (dict->table->array[i] != 0) {
      list_node_t *head = dict->table->array[i];
      while (head != NULL) {
        printf("%s\n", head->word);
        head = head->next;
      }
    }
  }
}

void dict_free(dictionary_t *dict) {
  if (dict->table->array != NULL) {
    list_node_t *head = dict->table->array[0];
    for (unsigned int i = 0; i < dict->table->length; i++) {
      if (dict->table->array[i] != 0) {
        head = dict->table->array[i];
        while (head != NULL) {
          list_node_t *tmp = head;
          head = tmp->next;
          free(tmp);
        }
      }
    }
  }

  free(dict->table->array);
  free(dict->table);
  free(dict);
}

dictionary_t *read_dict_from_text_file(const char *file_name) {
  FILE *file;
  char word[MAX_WORD_LEN];
  dictionary_t *dict = create_dictionary();
  int error;

  if (dict == NULL) {
    return NULL;
  }

  file = fopen(file_name, "r");

  if (file == NULL) {
    dict_free(dict);
    return NULL;
  }

  while (fscanf(file, "%s", word) != EOF) {
    error = dict_insert(dict, word);

    if (error) {
      dict_free(dict);
      fclose(file);
      return NULL;
    }
  }

  fclose(file);
  return dict;
}

int write_dict_to_text_file(const dictionary_t *dict, const char *file_name) {
  FILE *file;
  file = fopen(file_name, "w");
  if (file == NULL) {
    return 1;
  }
  list_node_t *head = dict->table->array[0];

  for (unsigned int i = 0; i < dict->table->length; ++i) {
    if (dict->table->array[i] != 0) {
      head = dict->table->array[i];
      while (head != NULL) {
        fprintf(file, "%s\n", head->word);
        head = head->next;
      }
    }
  }

  fclose(file);
  free(head);
  return 0;
}
